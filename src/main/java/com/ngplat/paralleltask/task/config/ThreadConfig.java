/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.task.config
 * @filename: ThreadConfig.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.task.config;

/** 
 * @typename: ThreadConfig
 * @brief: 任务全局配置参数
 * @author: KI ZCQ
 * @date: 2018年4月23日 下午4:56:39
 * @version: 1.0.0
 * @since
 * 
 */
public interface ThreadConfig {

	// 任务超时时间
	long TASK_TIMEOUT_MILL_SECONDS = 15 * 1000L;
	
	// 任务队列满时, sleep时间
    int QUEUE_FULL_SLEEP_TIME = 20;
    // 最大缓存任务数
    int MAX_CACHE_TASK_NUM = 2;
    
    // 核心任务数 - 对应corePoolSize - 核心线程数
    int CORE_TASK_NUM = Math.min(10, Runtime.getRuntime().availableProcessors());
    // 最大任务数 - 对应maximumPoolSize - 线程池所能容纳的最大线程数，超过这个数的线程将被阻塞
    int MAX_TASK_NUM = Math.max(15, Runtime.getRuntime().availableProcessors());
    
    // 暂时不用
    long NOT_LIMIT = -1;
	
}
