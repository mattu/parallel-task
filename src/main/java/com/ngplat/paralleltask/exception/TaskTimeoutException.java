/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.exception
 * @filename: TaskTimeoutException.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.exception;

/**
 * @typename: TaskTimeoutException
 * @brief: 任务执行超时异常
 * @author: KI ZCQ
 * @date: 2018年5月24日 下午7:16:19
 * @version: 1.0.0
 * @since
 *
 */
public class TaskTimeoutException extends RuntimeException {

    private static final long serialVersionUID = -7375423850222016116L;

    public TaskTimeoutException(String msg) {
        super(msg);
    }

    public TaskTimeoutException(Throwable cause) {
        super(cause);
    }

    public TaskTimeoutException(String message, Throwable cause) {
        super(message, cause);
    }
}
