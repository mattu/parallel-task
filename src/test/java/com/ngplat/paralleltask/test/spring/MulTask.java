/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.test.spring
 * @filename: MulTask.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.test.spring;

import com.ngplat.paralleltask.annotation.TaskBean;
import com.ngplat.paralleltask.exception.TaskExecutionException;
import com.ngplat.paralleltask.task.TaskContext;
import com.ngplat.paralleltask.task.TaskProcessor;

/** 
 * @typename: MulTask
 * @brief: 〈一句话功能简述〉
 * @author: KI ZCQ
 * @date: 2018年5月26日 下午3:45:55
 * @version: 1.0.0
 * @since
 * 
 */
@TaskBean(taskId = "3", name = "MulTask", status="D")
public class MulTask extends TaskProcessor {

	// serialVersionUID
	private static final long serialVersionUID = -9182874171029352165L;

	/**
	 * 创建一个新的实例 MulTask.
	 */
	public MulTask() {
		// TODO Auto-generated constructor stub
	}
	
	/** 
	 * @see com.ngplat.paralleltask.task.TaskProcessor#afterExecute()
	 */
	@Override
	public void afterExecute(TaskContext context) {
		System.out.println("确定可以执行MulTask的afterExecute.");
		super.afterExecute(context);
	}

	/** 
	 * @see com.ngplat.paralleltask.task.TaskProcessor#runWorker(com.ngplat.paralleltask.task.TaskContext)
	 */
	@Override
	public void runWorker(TaskContext context) throws TaskExecutionException {
		System.out.println(String.format("[MulTask] runWorker, result: %d", (10 * 8)));
	}

}
